import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('dataset.csv')

''' Q1 '''
for year in range(1396, 1401):
    for month in range(1, 13):
        c = ''
        if month < 10:
            c = '0'
        holiday_means = df[(df.is_holiday == True) &
                           (df.date <= f"{year}/{c}{month}/31") &
                           (df.date >= f"{year}/{c}{month}/01")
                           ].peak.mean()

        working_means = df[(df.is_holiday == False) &
                           (df.date <= f"{year}/{c}{month}/31") &
                           (df.date >= f"{year}/{c}{month}/01")
                           ].peak.mean()

        diff_in_holiday = df[(df.date <= f"{year}/{c}{month}/31") &
                             (df.date >= f"{year}/{c}{month}/01")
                             ].peak - holiday_means

        diff_in_working = df[(df.date <= f"{year}/{c}{month}/31") &
                             (df.date >= f"{year}/{c}{month}/01")
                             ].peak - working_means

        df.loc[df[(df.date <= f"{year}/{c}{month}/31") &
                  (df.date >= f"{year}/{c}{month}/01")
                  ].index, 'p_holiday_interpolated'] = diff_in_holiday

        df.loc[df[(df.date <= f"{year}/{c}{month}/31") &
                  (df.date >= f"{year}/{c}{month}/01")
                  ].index, 'p_working_interpolated'] = diff_in_working
# print(df.to_string())

# ----------------------------------------------------------------------------------------------------------------------

''' Q2 '''

dfm = df.iloc[::2, [0, 8]].copy(deep=True)


for i in df.index[::2]:
    dfm._set_value(i, 'p_holiday_interpolated', (df.p_holiday_interpolated[i] + df.p_holiday_interpolated[i + 1]) / 2)
    dfm._set_value(i, 'p_working_interpolated', (df.p_working_interpolated[i] + df.p_working_interpolated[i + 1]) / 2)

    # for Q4
    dfm._set_value(i, 'peak', (df.peak[i] + df.peak[i + 1]) / 2)

dfm.reset_index(inplace=True, drop=True)
# print(dfm.to_string())

# ----------------------------------------------------------------------------------------------------------------------

''' Q3 '''

for i in dfm.index:
    if dfm.is_holiday[i] == False:
        '''( daily peak mean - holiday_means) in working day --> p_difference '''
        dfm._set_value(i, 'p_difference', dfm.p_holiday_interpolated[i])

# print(dfm.to_string())

# ----------------------------------------------------------------------------------------------------------------------

''' Q4 '''

print('Electricity consumption of offices in 1400/05:',
      dfm[(dfm.date <= "1400/05/31") & (dfm.date >= "1400/05/01")].p_difference.sum(),
      '(Avg = {})'.format(round(dfm[(dfm.date <= "1400/05/31") & (dfm.date >= "1400/05/01")].p_difference.mean(), 2)))

print('Electricity consumption on a holiday in 1400/05:', dfm.peak[209])
print('Electricity consumption on a working day in 1400/05:', dfm.peak[206])

# ----------------------------------------------------------------------------------------------------------------------

''' Q5 '''

date = []
for i in dfm.index[::-1]:
    dfm._set_value(i, 'ym_num', int(dfm.date[i][:-3].replace('/', '')))
    date.append(int(dfm.date[i][:-3].replace('/', '')))

date = list(set(date))
date.sort()

daily_avg_offices = []
for year in range(1396, 1401):
    for month in range(1, 13):
        c = ''
        if month < 10:
            c = '0'
        daily_avg_offices.append(dfm[(dfm.date <= f"{year}/{c}{month}/31") &
                                     (dfm.date >= f"{year}/{c}{month}/01")
                                     ].p_difference.mean())


plt.scatter(x=daily_avg_offices, y=date)

# plt.show()

# ----------------------------------------------------------------------------------------------------------------------

''' Q6 '''

for i in dfm.index:
    dfm._set_value(i, 'year', int(dfm.date[i][:-6]))

# print(dfm.to_string())

int()
dfm[dfm.is_holiday == False].plot.scatter(x='peak', y='p_difference', c='year')
plt.xlabel("Total consumption")
plt.ylabel("Consumption of offices")

# plt.show()

# df.to_csv('df_Ex1.csv', encoding='utf-8', index=False)
# dfm.to_csv('dfm.csv', encoding='utf-8', index=False)
