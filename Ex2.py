import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('dataset.csv')

# 1
df.plot.scatter(x='peak', y='p_reserve', c='red')
plt.title("peak & p_reserve")


# 2
df.plot.scatter(x='peak', y='freq_avg', c='blue')
plt.title("peak & freq_avg")


# 3
df.plot.scatter(x='freq_avg', y='p_reserve', c='cyan')
plt.title("freq_avg & p_reserve")


# 4
for i in df.index:
    month_number = int(df.date[i][5:7])
    if 0 < month_number <= 3:
        s = 1
    if 3 < month_number <= 6:
        s = 2
    if 6 < month_number <= 9:
        s = 3
    if 9 < month_number <= 12:
        s = 4
    df._set_value(i, 's', s)

df.plot.scatter(x='peak', y='p_reserve', c='s')
plt.title("peak & p_reserve with season color")

df[(df.date <= '1397') & (df.date >= '1396')].plot.scatter(x='peak', y='p_reserve', c='s')
plt.title("peak & p_reserve in 1396-1397")

df[(df.date <= '1398') & (df.date >= '1397')].plot.scatter(x='peak', y='p_reserve', c='s')
plt.title("peak & p_reserve in 1397-1398")

df[(df.date <= '1399') & (df.date >= '1398')].plot.scatter(x='peak', y='p_reserve', c='s')
plt.title("peak & p_reserve in 1398-1399")

df[(df.date <= '1400') & (df.date >= '1399')].plot.scatter(x='peak', y='p_reserve', c='s')
plt.title("peak & p_reserve in 1399-1400")

df[(df.date <= '1401') & (df.date >= '1400')].plot.scatter(x='peak', y='p_reserve', c='s')
plt.title("peak & p_reserve in 1400-1401")


# 5
for i in df.index:
    flag = 0
    if df.p_reserve[i] == 0 and df.freq_avg[i] < 49.99:
        flag = 1
    df._set_value(i, 'widespread_off', flag)


# 6
dm_widespread_off = pd.DataFrame(columns=['date', 'min', 'max'])
for year in range(1396, 1401):
    for month in range(1, 13):
        c = ''
        if month < 10:
            c = '0'
        p_max = df[(df.widespread_off == 0.0) &
                   (df.date <= f"{year}/{c}{month}/31") &
                   (df.date >= f"{year}/{c}{month}/01")
                   ].peak.max()

        p_min = df[(df.widespread_off == 0.0) &
                   (df.date <= f"{year}/{c}{month}/31") &
                   (df.date >= f"{year}/{c}{month}/01")
                   ].peak.min()

        p_min_date = df[df.peak == (df[
                                        (df.widespread_off == 0.0) &
                                        (df.date <= f"{year}/{c}{month}/31") &
                                        (df.date >= f"{year}/{c}{month}/01")
                                        ].peak.min())].date.to_list()

        dm_widespread_off.loc[-1] = [p_min_date[0][:-3], p_min, p_max]  # adding a row
        dm_widespread_off.index = dm_widespread_off.index + 1  # shifting index
        dm_widespread_off = dm_widespread_off.sort_index()  # sorting by index


# df.to_csv('df_Ex2.csv', encoding='utf-8', index=False)
# dm_widespread_off.to_csv('dm_widespread_off.csv', encoding='utf-8', index=False)
plt.show()